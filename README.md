_______________________________________
        XORient Yourself
        
XOR is a popular "tool" used in cryptography.

Hopefully, this challenge gets you oriented  
to how cool XOR is.



### Walkthrough

Figure out that TUCTF is definitely in message, so find where that is which
gives away part of the key. Then use the partial key to crack more parts of the
message. Repeat until got flag.